import { Link } from 'gatsby'
import PropTypes from 'prop-types'
import React from 'react'
import './header.less'

const Header = ({ siteTitle }) => (
  <div className="layout-navigation">
    <div>
      <Link to="/">
        About
      </Link>
      <Link to="/projects">
        Projects
      </Link>
      <Link to="/resume">
        Resume
      </Link>
      <Link to="/contact">
        Contact
      </Link>
    </div>
  </div>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: '',
}

export default Header
