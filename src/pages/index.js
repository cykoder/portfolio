import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import Image from '../components/image'

import './index.less'

const IndexPage = () => (
  <Layout>
    <div className="page-index-header">
      <Image />
      <h1>
        Hello, I'm <strong>Sam Hellawell</strong> and<br />
        I develop websites, apps and games.
      </h1>
    </div>
    <div className="gallery">
      
    </div>
  </Layout>
)

export default IndexPage
